<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::get('/users', 'UserController@index');

Route::resource('/categories', 'CategoryController');

Route::resource('/words', 'WordController');
Route::post('/words/csv_import', 'WordController@import');
Route::get('/wordsDatatable', 'WordController@data');
