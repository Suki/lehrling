<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Word extends Model
{
    
	use SoftDeletes;

	public function category()
	{
		return $this->belongsTo('App\Category');
	}
	
}
