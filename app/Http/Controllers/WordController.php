<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Word;
use App\Category;
use Yajra\Datatables\Datatables;

class WordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('words.index');
    }

    /**
     * Data for Datatables
     */
    public function data()
    {
        $aux = Datatables::of(Word::query())->make(true);
        return $aux;
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function import(Request $request)
    {
        // Truncate Table?
        
        if (($handle = fopen(request()->file('csv_file'), 'r')) !== false)
        {

            $number = 1;
            $repeatedWords = 0;
            

            while (($row = fgetcsv($handle, 999, ',')) !== false)
            {
                //  $row[0] = Correlative number
                //  $row[1] = English
                //  $row[2] = German
                //  $row[3] = Spanish

                if($number > 1){

                    $word = New Word();
                    $word->englishWord = $row[1];
                    $word->germanWord = $row[2];
                    $word->spanishWord = $row[3];
                    $word->status = 1;
                    $word->save();

                }
                

            }
        }
        fclose($handle);

        return redirect('words');

    }


}
