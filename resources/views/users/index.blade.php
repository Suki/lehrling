@extends('layouts.main')

@section('content')
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<strong class="card-title">All users</strong>
					</div>
					<div class="card-body">
			  <table id="bootstrap-data-table" class="table table-striped table-bordered">
				<thead>
				  <tr>
					<th>Name</th>
					<th>Position</th>
					<th>Office</th>
					<th>Salary</th>
				  </tr>
				</thead>
				<tbody>
					@foreach($users as $user)
						<tr>
							<td>{{ $user->name }}</td>
						  </tr>
					@enforeach
				</tbody>
			  </table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .animated -->
@endsection