@extends('layouts.main_lite')

@section('content')
<div class="animated fadeIn">
    <div class="row">
        <div class="col-md-12">

            <div class="card">
                <div class="card-body">

                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal_create">
                        <i class="fa fa-plus-square"></i>&nbsp; Create
                    </button>

                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal_csv">
                        <i class="fa fa-upload"></i>&nbsp; Import
                    </button>

                    <a href="{{ asset('csv/import_template.csv') }}">
                        <button type="button" class="btn btn-outline-primary">
                            <i class="fa fa-download"></i>&nbsp;  CSV Template
                        </button>
                    </a>

                </div> 
            </div>

            <div class="card">
                <div class="card-body">
                    <table id="words-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>English</th>
                                <th>German</th>
                                <th>Spanish</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_csv" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <form class="form-horizontal" method="POST" action="{{url('/words/csv_import')}}" enctype="multipart/form-data">
        {{ csrf_field() }}
        @if(count($errors))
        <div class="col-md-12">
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @endif

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Import CSV</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row form-group">
                        <div class="col col-md-3"><label for="file-input" class=" form-control-label">File input</label></div>
                        <div class="col-12 col-md-9"><input type="file" id="csv_file" name="csv_file" class="form-control-file"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
    </form>
</div>

<div class="modal fade" id="modal_create" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <form class="form-horizontal" method="POST" action="{{url('/words/create')}}">
        {{ csrf_field() }}
        @if(count($errors))
        <div class="col-md-12">
            <div class="form-group">
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        @endif

        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="largeModalLabel">Create Word</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">English</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-english" name="text-english" class="form-control">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">German</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-german" name="text-german" class="form-control">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="text-input" class=" form-control-label">Spanish</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="text" id="text-spanish" name="text-spanish" class="form-control">
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-primary">Send</button>
                </div>
            </div>
        </div>
    </form>
</div>

@endsection

@section('script')
    
    <script type="text/javascript">
        
       jQuery(document).ready(function($) {
            $('#words-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! url('wordsDatatable') !!}',
                columns: [
                    { data: 'englishWord', name: 'englishWord' },
                    { data: 'germanWord', name: 'germanWord' },
                    { data: 'spanishWord', name: 'spanishWord' }
                ]
            });
        });
        
    </script>
@endsection
