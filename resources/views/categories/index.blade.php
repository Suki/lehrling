@extends('layouts.main')

@section('content')
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-body">
						<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-target="#modal"><i class="fa fa-plus-square"></i>&nbsp; Create</button>
					</div>
				</div>

				<div class="card">
					<div class="card-header">
						<strong class="card-title">All Categories</strong>
					</div>
					<div class="card-body">
						<table id="bootstrap-data-table" class="table table-striped table-bordered">
							<thead>
								<tr>
									<th>English</th>
									<th>German</th>
									<th>Spanish</th>
									<th>Options</th>
								</tr>
							</thead>
							<tbody>
							@foreach ($categories as $category)
								<tr>
									<td>{{ $category->nameEnglish }}</td>
									<td>{{ $category->nameGerman }}</td>
									<td>{{ $category->nameSpanish }}</td>
									<td></td>
								</tr>
							@endforeach	
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div><!-- .animated -->
	
	<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="largeModalLabel">Create Category</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					
					<div class="col-lg-6">
                    <div class="card">
                      <div class="card-header"><strong>Company</strong><small> Form</small></div>
                      <div class="card-body card-block">
                        <div class="form-group"><label for="company" class=" form-control-label">Company</label><input type="text" id="company" placeholder="Enter your company name" class="form-control"></div>
                        <div class="form-group"><label for="vat" class=" form-control-label">VAT</label><input type="text" id="vat" placeholder="DE1234567890" class="form-control"></div>
                        <div class="form-group"><label for="street" class=" form-control-label">Street</label><input type="text" id="street" placeholder="Enter street name" class="form-control"></div>
                        <div class="row form-group">
                          <div class="col-8">
                            <div class="form-group"><label for="city" class=" form-control-label">City</label><input type="text" id="city" placeholder="Enter your city" class="form-control"></div>
                          </div>
                          <div class="col-8">
                            <div class="form-group"><label for="postal-code" class=" form-control-label">Postal Code</label><input type="text" id="postal-code" placeholder="Postal Code" class="form-control"></div>
                          </div>
                        </div>
                        <div class="form-group"><label for="country" class=" form-control-label">Country</label><input type="text" id="country" placeholder="Country name" class="form-control"></div>
                      </div>
                    </div>
                  </div>
					
					
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary">Send</button>
				</div>
			</div>
		</div>
	</div>
@endsection