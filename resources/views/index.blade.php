@extends('layouts.main_lite')

@section('content')
    <h1>Dashboard - ToDo</h1>
    <div class="row">

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>General</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>Fix fonts when offline</li>
                        <li>Dashboard Design</li>
                    </ul>
                </div>
            </div>
        </div>
       
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Categories</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>Index -> Datatables</li>
                        <li>Create -> Modal</li>
                        <li>Edit -> Modal</li>
                        <li>Delete -> Modal</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Users</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>Index -> Datatables</li>
                        <li>Create -> Modal</li>
                        <li>Edit -> Modal</li>
                        <li>Delete -> Modal</li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h4>Words</h4>
                </div>
                <div class="card-body">
                    <ul class="list-unstyled">
                        <li>Index -> Datatables</li>
                        <li>Create -> Modal</li>
                        <li>Edit -> Modal</li>
                        <li>Delete -> Modal</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
@endsection