<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('words', function (Blueprint $table) {
            $table->increments('id');
      			$table->string('englishWord')->nullable();
      			$table->string('germanWord')->nullable();
      			$table->string('spanishWord')->nullable();
      			$table->string('englishPhrase')->nullable();
      			$table->string('germanPhrase')->nullable();
      			$table->string('spanishPhrase')->nullable();
      			$table->string('category_id')->nullable();
      			$table->boolean('status');
            $table->timestamps();
			      $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('words');
    }
}
