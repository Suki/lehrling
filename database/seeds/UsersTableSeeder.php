<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		
		DB::table('users')->insert([
            'name' => 'Ana',
            'email' => 'usuario@lehrling.sec',
            'password' => bcrypt('secret'),
        ]);
		
		DB::table('users')->insert([
            'name' => 'Sebastian',
            'email' => 'sebastian@lehrling.sec',
            'password' => bcrypt('secret'),
        ]);
		
		DB::table('users')->insert([
            'name' => 'Catalina',
            'email' => 'catalina@lehrling.sec',
            'password' => bcrypt('secret'),
        ]);
		
    }
}
