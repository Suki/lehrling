<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
					'nameEnglish' => 'Numbers',
					'nameGerman' => 'Zahlen',
					'nameSpanish' => 'Números'
		]);
		
		Category::create([
					'nameEnglish' => 'Adjetives',
					'nameGerman' => 'Zahlen',
					'nameSpanish' => 'Adjetivos'
		]);
		
		Category::create([
					'nameEnglish' => 'Verbs',
					'nameGerman' => 'Zahlen',
					'nameSpanish' => 'Verbos'
		]);
		
    }
}
