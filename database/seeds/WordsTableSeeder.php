<?php

use Illuminate\Database\Seeder;
use App\Word;

class WordsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
		Word::create([
					'englishWord' => 'Yes',
					'germanWord' => 'Ja',
					'spanishWord' => 'Si',
					'status' => '1'
		]);
    }
}
